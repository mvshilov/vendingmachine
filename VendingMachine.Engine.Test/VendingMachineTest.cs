﻿using System;
using System.Text;
using System.Collections.Generic;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace VendingMachine.Engine.Test
{
    [TestClass]
    public class VendingMachineTest
    {
        [TestMethod]
        public void ConstructorTest()
        {
            var vm = new VendingMachine();

            // money storage initial test
            vm.InnerMoneyStorage[Coins.OneRouble].Should().Be(0);
            vm.InnerMoneyStorage[Coins.TwoRoubles].Should().Be(0);
            vm.InnerMoneyStorage[Coins.FiveRoubles].Should().Be(0);
            vm.InnerMoneyStorage[Coins.TenRoubles].Should().Be(0);

            // products storage initial state
            vm.ProductsStorage.Length.Should().Be(4);

            vm.ProductsStorage[0].Should().BeNull();
            vm.ProductsStorage[1].Should().BeNull();
            vm.ProductsStorage[2].Should().BeNull();
            vm.ProductsStorage[3].Should().BeNull();

            // Inserted
            vm.Inserted.Should().Be(0);
        }

        [TestMethod]
        public void InsertAndMoneyBackTest()
        {
            var vm = new VendingMachine();

            // insert
            var initial1RoubleQty = vm.InnerMoneyStorage[Coins.OneRouble];
            vm.InsertCoin(Coins.OneRouble);
            vm.Inserted.Should().Be(1);
            vm.InnerMoneyStorage[Coins.OneRouble].Should().Be(initial1RoubleQty + 1);

            var initial5RoublesQty = vm.InnerMoneyStorage[Coins.FiveRoubles];
            vm.InsertCoin(Coins.FiveRoubles);
            vm.InsertCoin(Coins.FiveRoubles);
            vm.InsertCoin(Coins.FiveRoubles);
            vm.Inserted.Should().Be(16);
            vm.InnerMoneyStorage[Coins.FiveRoubles].Should().Be(initial5RoublesQty + 3);

            // money back
            var coinsGotBack = vm.GetMoneyBack();
            coinsGotBack.Count.Should().Be(4);
            vm.Inserted.Should().Be(0);
            vm.InnerMoneyStorage[Coins.OneRouble].Should().Be(initial1RoubleQty);
            vm.InnerMoneyStorage[Coins.FiveRoubles].Should().Be(initial5RoublesQty);

            coinsGotBack[0].Should().Be(Coins.OneRouble);
            coinsGotBack[1].Should().Be(Coins.FiveRoubles);
            coinsGotBack[2].Should().Be(Coins.FiveRoubles);
            coinsGotBack[3].Should().Be(Coins.FiveRoubles);
        }

        [TestMethod]
        public void BuyProductTest()
        {
            var vm = new VendingMachine();
            vm.ProductsStorage[0] = new Product("Чай", 13) {Qty = 5};

            // no coins inserted
            Action tryToBuyForFree = () => vm.BuyProduct(0);
            tryToBuyForFree.ShouldThrow<NotEnoughMoneyException>();

            // no coins inserted
            vm.InsertCoin(Coins.FiveRoubles);
            Action tryToBuyForLessPrice = () => vm.BuyProduct(0);
            tryToBuyForLessPrice.ShouldThrow<NotEnoughMoneyException>();

            // add enough coins
            vm.InsertCoin(Coins.FiveRoubles);
            vm.InsertCoin(Coins.TwoRoubles);
            vm.InsertCoin(Coins.OneRouble);
            vm.BuyProduct(0);
            vm.ProductsStorage[0].Qty.Should().Be(4);
            vm.Inserted.Should().Be(0);
            vm.InnerMoneyStorage[Coins.OneRouble].Should().Be(1);
            vm.InnerMoneyStorage[Coins.TwoRoubles].Should().Be(1);
            vm.InnerMoneyStorage[Coins.FiveRoubles].Should().Be(2);
        }

        [TestMethod]
        public void ProductNotInStockTest()
        {
            var vm = new VendingMachine();

            vm.ProductsStorage[0] = new Product("Чай", 13);
            vm.ProductsStorage[0].Qty.Should().Be(0);

            vm.InsertCoin(Coins.FiveRoubles);
            vm.InsertCoin(Coins.FiveRoubles);
            vm.InsertCoin(Coins.TwoRoubles);
            vm.InsertCoin(Coins.OneRouble);

            Action tryToBuyProductNotInStock = () => vm.BuyProduct(0);
            tryToBuyProductNotInStock.ShouldThrow<ProductNotInStockException>();
        }

        [TestMethod]
        public void BuyProductAndGetChangeTest()
        {
            var vm = new VendingMachine();
            vm.InnerMoneyStorage.PutCoins(Coins.TwoRoubles, 2);

            vm.ProductsStorage[2] = new Product("Кофе с молоком", 21) {Qty = 1};

            vm.InsertCoin(Coins.FiveRoubles);
            vm.InsertCoin(Coins.TenRoubles);
            vm.InsertCoin(Coins.TenRoubles);

            vm.BuyProduct(2);

            // Get change
            var change = vm.GetMoneyBack();
            change.Count.Should().Be(2);

        }

        [TestMethod]
        public void BuyProductUnableToGetChangeTest()
        {
            var vm = new VendingMachine();
            
            vm.InnerMoneyStorage.PutCoins(Coins.FiveRoubles, 100);
            vm.InnerMoneyStorage.PutCoins(Coins.TenRoubles, 100);

            vm.ProductsStorage[2] = new Product("Кофе с молоком", 21) {Qty = 1};

            vm.InsertCoin(Coins.FiveRoubles);
            vm.InsertCoin(Coins.TenRoubles);
            vm.InsertCoin(Coins.TenRoubles);

            // required change - 4 roubles. No way to give it without 1 and 2 rouble coins
            Action tryToBuyProuct = () => vm.BuyProduct(2);
            tryToBuyProuct.ShouldThrow<UnableToGiveAChangeException>();
        }
    }
}
