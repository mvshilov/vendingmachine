﻿using System;
using System.Text;
using System.Collections.Generic;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace VendingMachine.Engine.Test
{
    [TestClass]
    public class WalletManagerTest
    {
        [TestMethod]
        public void NotEnoughMoneyTest()
        {
            var wallet = new Wallet();
            wallet.PutCoins(Coins.TwoRoubles, 1);
            wallet.PutCoins(Coins.TenRoubles, 2);

            Action tryGetMoreMoneyThanWalletContains = () => new WalletManager(wallet).GetCoins(100500);
            tryGetMoreMoneyThanWalletContains.ShouldThrow<NotEnoughMoneyException>();

            // wallet state not changed
            wallet[Coins.OneRouble].Should().Be(0);
            wallet[Coins.TwoRoubles].Should().Be(1);
            wallet[Coins.FiveRoubles].Should().Be(0);
            wallet[Coins.TenRoubles].Should().Be(2);
        }

        [TestMethod]
        public void GetCoins_InfiniteCoins_13_Test()
        {
            var wallet = GetInfiniteWallet();

            var coins = new WalletManager(wallet).GetCoins(13);

            coins.Length.Should().Be(3);
            coins[0].Should().Be(Coins.TenRoubles);
            coins[1].Should().Be(Coins.TwoRoubles);
            coins[2].Should().Be(Coins.OneRouble);

            // check wallet state
            wallet[Coins.OneRouble].Should().Be(int.MaxValue - 1);
            wallet[Coins.TwoRoubles].Should().Be(int.MaxValue - 1);
            wallet[Coins.FiveRoubles].Should().Be(int.MaxValue);
            wallet[Coins.TenRoubles].Should().Be(int.MaxValue - 1);
        }

        [TestMethod]
        public void GetCoins_InfiniteCoins_40_Test()
        {
            var wallet = GetInfiniteWallet();

            var coins = new WalletManager(wallet).GetCoins(40);

            coins.Length.Should().Be(4);
            coins[0].Should().Be(Coins.TenRoubles);
            coins[1].Should().Be(Coins.TenRoubles);
            coins[2].Should().Be(Coins.TenRoubles);
            coins[3].Should().Be(Coins.TenRoubles);

            // check wallet state
            wallet[Coins.OneRouble].Should().Be(int.MaxValue);
            wallet[Coins.TwoRoubles].Should().Be(int.MaxValue);
            wallet[Coins.FiveRoubles].Should().Be(int.MaxValue);
            wallet[Coins.TenRoubles].Should().Be(int.MaxValue - 4);
        }

        [TestMethod]
        public void GetCoins_LimitedCoins_5_Test()
        {
            var wallet = new Wallet();
            wallet.PutCoins(Coins.OneRouble, 20);
            wallet.PutCoins(Coins.TwoRoubles, 1);

            var coins = new WalletManager(wallet).GetCoins(5);

            coins.Length.Should().Be(4);
            coins[0].Should().Be(Coins.TwoRoubles);
            coins[1].Should().Be(Coins.OneRouble);
            coins[2].Should().Be(Coins.OneRouble);
            coins[3].Should().Be(Coins.OneRouble);

            // check wallet state
            wallet[Coins.OneRouble].Should().Be(17);
            wallet[Coins.TwoRoubles].Should().Be(0);
            wallet[Coins.FiveRoubles].Should().Be(0);
            wallet[Coins.TenRoubles].Should().Be(0);
        }

        [TestMethod]
        public void GetCoins_LimitedCoins_5_UnableToGive_Test()
        {
            var wallet = new Wallet();
            wallet.PutCoins(Coins.TenRoubles, 20);

            Action tryGetCoins = () => new WalletManager(wallet).GetCoins(5);
            tryGetCoins.ShouldThrow<UnableToGiveSuchAmountException>();

            // wallet state not changed
            wallet[Coins.OneRouble].Should().Be(0);
            wallet[Coins.TwoRoubles].Should().Be(0);
            wallet[Coins.FiveRoubles].Should().Be(0);
            wallet[Coins.TenRoubles].Should().Be(20);
        }

        [TestMethod]
        public void GetCoins_LimitedCoins_13_UnableToGive_Test()
        {
            var wallet = new Wallet();
            wallet.PutCoins(Coins.TwoRoubles, 100);
            wallet.PutCoins(Coins.FiveRoubles, 100);
            wallet.PutCoins(Coins.TenRoubles, 100);

            Action tryGetCoins = () => new WalletManager(wallet).GetCoins(13);
            tryGetCoins.ShouldThrow<UnableToGiveSuchAmountException>();

            // wallet state not changed
            wallet[Coins.OneRouble].Should().Be(0);
            wallet[Coins.TwoRoubles].Should().Be(100);
            wallet[Coins.FiveRoubles].Should().Be(100);
            wallet[Coins.TenRoubles].Should().Be(100);
        }

        private Wallet GetInfiniteWallet()
        {
            var result = new Wallet();

            result.PutCoins(Coins.OneRouble, int.MaxValue);
            result.PutCoins(Coins.TwoRoubles, int.MaxValue);
            result.PutCoins(Coins.FiveRoubles, int.MaxValue);
            result.PutCoins(Coins.TenRoubles, int.MaxValue);

            return result;
        }
    }
}
