﻿using System;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace VendingMachine.Engine.Test
{
    [TestClass]
    public class WalletTest
    {
        [TestMethod]
        public void ConstructorTest()
        {
            var wallet = new Wallet();

            foreach (var coin in Coins.All)
            {
                wallet[coin].Should().Be(0);
            }

            wallet.Amount.Should().Be(0);
        }

        [TestMethod]
        public void PutCoinTest()
        {
            var wallet = new Wallet();

            // from scratch
            wallet.PutCoins(Coins.OneRouble, 3);
            wallet.PutCoins(Coins.TwoRoubles, 2);

            wallet[Coins.OneRouble].Should().Be(3);
            wallet[Coins.TwoRoubles].Should().Be(2);

            wallet.Amount.Should().Be(7);

            // add
            wallet.PutCoins(Coins.OneRouble, 1);
            wallet.PutCoins(Coins.TwoRoubles, 3);

            wallet[Coins.OneRouble].Should().Be(4);
            wallet[Coins.TwoRoubles].Should().Be(5);

            //wallet.Amount.Should().Be(7);
        }

        [TestMethod]
        public void PickCoinTest()
        {
            var wallet = new Wallet();
            wallet.PutCoins(Coins.FiveRoubles, 5);

            wallet.PickCoins(Coins.FiveRoubles, 3);
            wallet[Coins.FiveRoubles].Should().Be(2);

            // not enough coins
            Action action2 = (() => wallet.PickCoins(Coins.FiveRoubles, 10));
            action2.ShouldThrow<NotEnoughMoneyException>();
        }
    }
}
