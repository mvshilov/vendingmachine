﻿namespace VendingMachine.WinFormsUI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.VmFrontPane = new System.Windows.Forms.GroupBox();
            this.CustomersWalletPane = new System.Windows.Forms.GroupBox();
            this.VmDashboard = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TakeMoneyBackButton = new System.Windows.Forms.Button();
            this.InsertedLabel = new System.Windows.Forms.Label();
            this.VmFrontPane.SuspendLayout();
            this.SuspendLayout();
            // 
            // VmFrontPane
            // 
            this.VmFrontPane.Controls.Add(this.InsertedLabel);
            this.VmFrontPane.Controls.Add(this.TakeMoneyBackButton);
            this.VmFrontPane.Controls.Add(this.label1);
            this.VmFrontPane.Location = new System.Drawing.Point(12, 12);
            this.VmFrontPane.Name = "VmFrontPane";
            this.VmFrontPane.Size = new System.Drawing.Size(372, 293);
            this.VmFrontPane.TabIndex = 0;
            this.VmFrontPane.TabStop = false;
            this.VmFrontPane.Text = "Vending Machine";
            // 
            // CustomersWalletPane
            // 
            this.CustomersWalletPane.Location = new System.Drawing.Point(12, 323);
            this.CustomersWalletPane.Name = "CustomersWalletPane";
            this.CustomersWalletPane.Size = new System.Drawing.Size(610, 106);
            this.CustomersWalletPane.TabIndex = 1;
            this.CustomersWalletPane.TabStop = false;
            this.CustomersWalletPane.Text = "Customer\'s Wallet";
            // 
            // VmDashboard
            // 
            this.VmDashboard.Location = new System.Drawing.Point(390, 13);
            this.VmDashboard.Name = "VmDashboard";
            this.VmDashboard.Size = new System.Drawing.Size(232, 292);
            this.VmDashboard.TabIndex = 2;
            this.VmDashboard.TabStop = false;
            this.VmDashboard.Text = "Vending Machine Dashboard";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 222);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Внесенная сумма:";
            // 
            // TakeMoneyBackButton
            // 
            this.TakeMoneyBackButton.Location = new System.Drawing.Point(14, 247);
            this.TakeMoneyBackButton.Name = "TakeMoneyBackButton";
            this.TakeMoneyBackButton.Size = new System.Drawing.Size(125, 32);
            this.TakeMoneyBackButton.TabIndex = 2;
            this.TakeMoneyBackButton.Text = "вернуть деньги";
            this.TakeMoneyBackButton.UseVisualStyleBackColor = true;
            this.TakeMoneyBackButton.Click += new System.EventHandler(this.TakeMoneyBackButton_Click);
            // 
            // InsertedLabel
            // 
            this.InsertedLabel.AutoSize = true;
            this.InsertedLabel.Location = new System.Drawing.Point(142, 222);
            this.InsertedLabel.Name = "InsertedLabel";
            this.InsertedLabel.Size = new System.Drawing.Size(20, 17);
            this.InsertedLabel.TabIndex = 3;
            this.InsertedLabel.Text = "р.";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(633, 440);
            this.Controls.Add(this.VmDashboard);
            this.Controls.Add(this.CustomersWalletPane);
            this.Controls.Add(this.VmFrontPane);
            this.Name = "MainForm";
            this.Text = "Vending Machine Emulator";
            this.VmFrontPane.ResumeLayout(false);
            this.VmFrontPane.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox VmFrontPane;
        private System.Windows.Forms.GroupBox CustomersWalletPane;
        protected System.Windows.Forms.GroupBox VmDashboard;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button TakeMoneyBackButton;
        private System.Windows.Forms.Label InsertedLabel;
    }
}

