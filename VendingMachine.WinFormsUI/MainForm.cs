﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VendingMachine.Engine;

namespace VendingMachine.WinFormsUI
{
    public partial class MainForm : Form
    {
        private readonly Engine.VendingMachine _vm = new Engine.VendingMachine();

        private readonly Wallet _customersWallet = new Wallet();

        public MainForm()
        {
            InitializeComponent();

            InitializeVendingMachine();
            InitializeCustomersWallet();

            AddControlsForProductSelection();
            AddControlsForStateOfStocks();
            AddControlsForStateOfSMoneyStorage();
            AddControlsForCustomersWallet();

            DisplayVmState();
        }

        private void InitializeVendingMachine()
        {
            // дадим машине денег на сдачу
            _vm.InnerMoneyStorage.PutCoins(Coins.OneRouble, 100);
            _vm.InnerMoneyStorage.PutCoins(Coins.TwoRoubles, 100);
            _vm.InnerMoneyStorage.PutCoins(Coins.FiveRoubles, 100);
            _vm.InnerMoneyStorage.PutCoins(Coins.TenRoubles, 100);

            // и положим в нее товары
            _vm.ProductsStorage[0] = new Product("Чай", 13) { Qty = 10 };
            _vm.ProductsStorage[1] = new Product("Кофе", 18) { Qty = 20 };
            _vm.ProductsStorage[2] = new Product("Кофе с молоком", 21) { Qty = 20 };
            _vm.ProductsStorage[3] = new Product("Сок", 35) { Qty = 15 };
        }

        private void InitializeCustomersWallet()
        {
            // покупателю - деньги на покупки
            _customersWallet.PutCoins(Coins.OneRouble, 10);
            _customersWallet.PutCoins(Coins.TwoRoubles, 30);
            _customersWallet.PutCoins(Coins.FiveRoubles, 20);
            _customersWallet.PutCoins(Coins.TenRoubles, 15);
        }

        #region Controls Creation

        private void AddControlsForProductSelection()
        {
            var labelStartPoint = new Point(10, 30);
            var buttonStartPoint = new Point(170, 25);
            const int rowHeight = 30;

            for (int i = 0; i < _vm.ProductsStorage.Length; i++)
            {
                var product = _vm.ProductsStorage[i];

                var label = new Label();
                label.Name = $"ProductNameLabel_{i}";
                label.Text = $"{product.Name} ({product.Price} р.)";
                label.Location = new Point(labelStartPoint.X, labelStartPoint.Y + i*rowHeight);
                label.Size = new Size(140, 17);
                VmFrontPane.Controls.Add(label);

                var button = new Button();
                button.Location = new Point(buttonStartPoint.X, buttonStartPoint.Y + i*rowHeight);
                button.Name = $"BuyProductButton_{i}";
                button.Size = new Size(99, 27);
                button.Text = "купить";
                button.UseVisualStyleBackColor = true;

                var productNumber = i;
                button.Click += (s, e) => BuyProduct(productNumber);
                VmFrontPane.Controls.Add(button);
            }
        }

        private void AddControlsForStateOfStocks()
        {
            var productNameLabelStartPoint = new Point(10, 25);
            var productQtyLabelStartPoint = new Point(125, 25);
            const int rowHeight = 20;

            for (int i = 0; i < _vm.ProductsStorage.Length; i++)
            {
                var product = _vm.ProductsStorage[i];

                var productNameLabel = new Label();
                productNameLabel.Name = $"ProductNameStockLabel_{i}";
                productNameLabel.Text = $"{product.Name}: ";
                productNameLabel.Location = new Point(productNameLabelStartPoint.X,
                    productNameLabelStartPoint.Y + i*rowHeight);
                productNameLabel.Size = new Size(100, 17);
                VmDashboard.Controls.Add(productNameLabel);

                var productQtyLabel = new Label();
                productQtyLabel.Name = $"ProductQtyStockLabel_{i}";
                //productQtyLabel.Text = $"{product.Qty} шт.";
                productQtyLabel.Location = new Point(productQtyLabelStartPoint.X,
                    productQtyLabelStartPoint.Y + i*rowHeight);
                productQtyLabel.Size = new Size(45, 17);
                VmDashboard.Controls.Add(productQtyLabel);

            }
        }

        private void AddControlsForStateOfSMoneyStorage()
        {
            var coinNameLabelStartPoint = new Point(10, 130);
            var coinQtyLabelStartPoint = new Point(125, 130);
            const int rowHeight = 20;

            for (int i = 0; i < Coins.All.Length; i++)
            {
                var coin = Coins.All[i];

                var coinNameLabel = new Label();
                coinNameLabel.Name = $"CoinNameStockLabel_{i}";
                coinNameLabel.Text = $"{coin.Name}: ";
                coinNameLabel.Location = new Point(coinNameLabelStartPoint.X,
                    coinNameLabelStartPoint.Y + i*rowHeight);
                coinNameLabel.Size = new Size(100, 17);
                VmDashboard.Controls.Add(coinNameLabel);

                var coinsQtyLabel = new Label();
                coinsQtyLabel.Name = $"CoinQtyStockLabel_{i}";
                //coinsQtyLabel.Text = $"{_vm.InnerMoneyStorage[coin]} шт.";
                coinsQtyLabel.Location = new Point(coinQtyLabelStartPoint.X,
                    coinQtyLabelStartPoint.Y + i*rowHeight);
                coinsQtyLabel.Size = new Size(45, 17);
                VmDashboard.Controls.Add(coinsQtyLabel);

            }
        }

        private void AddControlsForCustomersWallet()
        {
            var coinNameLabelStartPoint = new Point(10, 20);
            var coinQtyLabelStartPoint = new Point(12, 63);
            const int colWidth = 50;

            for (int i = 0; i < Coins.All.Length; i++)
            {
                var coin = Coins.All[i];

                var insertCoinButton = new Button();
                insertCoinButton.Name = $"InsertCoinButton_{i}";
                insertCoinButton.Text = coin.Name;
                insertCoinButton.Location = new Point(coinNameLabelStartPoint.X + i * colWidth,
                    coinNameLabelStartPoint.Y);
                insertCoinButton.Size = new Size(40, 40);
                insertCoinButton.TextAlign = ContentAlignment.MiddleCenter;
                insertCoinButton.Click += (s, e) => InsertCoin(coin);
                CustomersWalletPane.Controls.Add(insertCoinButton);

                var coinsQtyLabel = new Label();
                coinsQtyLabel.Name = $"CustomerCoinsQty_{i}";
                //coinsQtyLabel.Text = $"{_customersWallet[coin]} шт.";
                coinsQtyLabel.Location = new Point(coinQtyLabelStartPoint.X + i * colWidth,
                    coinQtyLabelStartPoint.Y);
                coinsQtyLabel.Size = new Size(45, 17);
                CustomersWalletPane.Controls.Add(coinsQtyLabel);

            }
        }

        #endregion Controls Creation

        #region Display VM state

        private void DisplayVmState()
        {
            DisplayStocks();
            DisplayShowCoins();
            DisplayCoinsInCustomersWallet();
            InsertedLabel.Text = $"{_vm.Inserted} р.";
        }

        private void DisplayStocks()
        {
            for (int i = 0; i < _vm.ProductsStorage.Length; i++)
            {
                var product = _vm.ProductsStorage[i];

                var productQtyLabel = (Label)VmDashboard.Controls.Find($"ProductQtyStockLabel_{i}", false)[0];
                productQtyLabel.Text = $"{product.Qty} шт.";
            }
        }

        private void DisplayShowCoins()
        {
            for (int i = 0; i < Coins.All.Length; i++)
            {
                var coin = Coins.All[i];

                var coinQtyLabel = (Label)VmDashboard.Controls.Find($"CoinQtyStockLabel_{i}", false)[0];
                coinQtyLabel.Text = $"{_vm.InnerMoneyStorage[coin]} шт.";
            }
        }

        private void DisplayCoinsInCustomersWallet()
        {
            for (int i = 0; i < Coins.All.Length; i++)
            {
                var coin = Coins.All[i];

                var coinQtyLabel = (Label)CustomersWalletPane.Controls.Find($"CustomerCoinsQty_{i}", false)[0];
                coinQtyLabel.Text = $"{_customersWallet[coin]} шт.";

                var insertCoinButton = (Button)CustomersWalletPane.Controls.Find($"InsertCoinButton_{i}", false)[0];
                insertCoinButton.Enabled = _customersWallet[coin] > 0;
            }
        }

        #endregion Display VM state

        private void TakeMoneyBackButton_Click(object sender, EventArgs e)
        {
            TakeMoneyBack();
        }

        private void TakeMoneyBack()
        {
            _vm.GetMoneyBack().ForEach(c => _customersWallet.PutCoins(c, 1));
            DisplayVmState();
        }

        private void InsertCoin(Coin coin)
        {
            _customersWallet.PickCoins(coin, 1);
            _vm.InsertCoin(coin);

            DisplayVmState();
        }

        private void BuyProduct(int productId)
        {
            try
            {
                _vm.BuyProduct(productId);

                // если надо дать сдачу
                if (_vm.Inserted > 0)
                {
                    MessageBox.Show($"Получите сдачу {_vm.Inserted} р. Спасибо за покупку!");
                    TakeMoneyBack();
                }
                else
                {
                    MessageBox.Show("Спасибо за покупку!");
                }
            }
            catch (NotEnoughMoneyException)
            {
                MessageBox.Show("Недостаточно средств");
            }
            catch (ProductNotInStockException)
            {
                MessageBox.Show("Товара нет в наличии");
            }
            catch (UnableToGiveAChangeException)
            {
                MessageBox.Show("Недостаточно монет, чтобы дать сдачу");
                // тут в следующих версиях можно предложить купить товар без сдачи
            }

            DisplayVmState();
        }
    }
}
