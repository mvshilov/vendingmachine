﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine.Engine
{
    public class WalletManager
    {
        private readonly Wallet _wallet;

        public WalletManager(Wallet wallet)
        {
            _wallet = wallet;
        }

        public Coin[] GetCoins(decimal amount)
        {
            if (_wallet.Amount < amount)
            {
                throw new NotEnoughMoneyException();
            }

            var coinsSorted = Coins.All.Select(c => c).OrderByDescending(c => c.Value);

            var result = new List<Coin>();

            var balance = amount;

            foreach (var coin in coinsSorted)
            {
                while (_wallet[coin] > 0 && balance >= coin.Value)
                {
                    result.Add(coin);
                    balance -= coin.Value;
                    _wallet.PickCoins(coin, 1);
                }
            }

            if (balance > 0)
            {
                // put selected coins back
                result.ForEach(c => _wallet.PutCoins(c, 1));

                // throw the exception
                throw new UnableToGiveSuchAmountException();
            }

            return result.ToArray();
        }
    }

    public class UnableToGiveSuchAmountException : Exception { }
}
