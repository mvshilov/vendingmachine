﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine.Engine
{
    public static class Coins
    {
        public static readonly Coin OneRouble = new Coin("1р.", 1);
        public static readonly Coin TwoRoubles = new Coin("2р.", 2);
        public static readonly Coin FiveRoubles = new Coin("5р.", 5);
        public static readonly Coin TenRoubles = new Coin("10р.", 10);

        public static readonly Coin[] All =
        {
            OneRouble,
            TwoRoubles,
            FiveRoubles,
            TenRoubles
        };
    }
}
