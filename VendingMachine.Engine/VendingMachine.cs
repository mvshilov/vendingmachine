﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine.Engine
{
    public class VendingMachine
    {
        // В первой версии понадеемся на честность людей и поверим, что они не выгребут из машины все деньги))
        // По уму надо возвращать клон кошелька, но спешить тоже надо, и это сейчас важнее
        public Wallet InnerMoneyStorage { get; }

        private List<Coin> _insertedCoins = new List<Coin>();

        // Со складом продуктов та же драма, что и с деньгохранилищем
        public Product[] ProductsStorage { get; }

        public VendingMachine()
        {
            InnerMoneyStorage = new Wallet();

            // VM содержит не больше 4 разных типов товаров
            ProductsStorage = new Product[] {null, null, null, null};
        }

        public decimal Inserted
        {
            get { return _insertedCoins.Sum(c => c.Value); }
        }

        public void InsertCoin(Coin coin)
        {
            _insertedCoins.Add(coin);
            InnerMoneyStorage.PutCoins(coin, 1);
        }

        public List<Coin> GetMoneyBack()
        {
            var result = new Coin[_insertedCoins.Count];

            _insertedCoins.ForEach(c => InnerMoneyStorage.PickCoins(c, 1));
            _insertedCoins.CopyTo(result);
            _insertedCoins.Clear();

            return result.ToList();
        }

        public void BuyProduct(int productNumber)
        {
            var product = ProductsStorage[productNumber];

            if (product == null || product.Qty == 0)
            {
                throw new ProductNotInStockException();
            }

            if (product.Price > Inserted)
            {
                throw new NotEnoughMoneyException();
            }

            var isChangeRequired = (product.Price < Inserted);
            if (isChangeRequired)
            {
                var walletManager = new WalletManager(InnerMoneyStorage);

                Coin[] change;
                try
                {
                    // пытаемся извлечь сдачу из деньгохранилища
                    change = walletManager.GetCoins(Inserted - product.Price);
                }
                catch (UnableToGiveSuchAmountException)
                {
                    // сдачу набрать не удастся - это плохо.
                    throw new UnableToGiveAChangeException();
                }

                // проверили - сдачу дать можно. Это хорошо, возвращаем сдачу в деньгохранилище
                change.ToList().ForEach(c => InnerMoneyStorage.PutCoins(c, 1));

                // забрали из "монетопиемника" сумму равную цене продукта, сдачу оставили
                _insertedCoins.Clear();
                change.ToList().ForEach(c => _insertedCoins.Add(c));

                // и вернули с
            }
            else
            {
                _insertedCoins.Clear();
            }

            product.Qty -= 1;
        }
    }

    public class NotEnoughMoneyException : Exception { }
    public class ProductNotInStockException : Exception { }
    public class UnableToGiveAChangeException : Exception { }
}
