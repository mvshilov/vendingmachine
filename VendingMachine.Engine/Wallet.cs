﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace VendingMachine.Engine
{
    public class Wallet
    {
        private readonly Dictionary<Coin, int> _innerStorage = new Dictionary<Coin, int>();

        public Wallet()
        {
            foreach (var coin in Coins.All)
            {
                this[coin] = 0;
            }
        }

        public int this[Coin key]
        {
            get
            {
                return _innerStorage[key];
            }
            protected set
            {
                _innerStorage[key] = value;
            }
        }

        public decimal Amount
        {
            get
            {
                return _innerStorage.Keys.Sum(coin => this[coin] * coin.Value);
            }
        }

        public void PutCoins(Coin coin, int coinsQty)
        {
            if (coinsQty <= 0)
            {
                throw new ArgumentException();
            }

            this[coin] += coinsQty;
        }

        public void PickCoins(Coin coin, int coinsQty)
        {
            if (this[coin] < coinsQty)
            {
                throw new NotEnoughMoneyException();
            }

            if (coinsQty <= 0)
            {
                throw new ArgumentException();
            }
            this[coin] -= coinsQty;
        }
    }
}
