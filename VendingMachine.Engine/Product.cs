﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine.Engine
{
    public class Product
    {
        public string Name { get; }
        public decimal Price { get; }
        public int Qty { get; set; }

        public Product(string name, decimal price)
        {
            Name = name;
            Price = price;
            Qty = 0;
        }

        //public override bool Equals(object obj)
        //{
        //    if (obj == null)
        //    {
        //        return false;
        //    }

        //    var product = obj as Product;

        //    if (product == null)
        //    {
        //        return false;
        //    }

        //    if (product.Name != Name)
        //    {
        //        return false;
        //    }

        //    if (product.Price != Price)
        //    {
        //        return false;
        //    }

        //    return true;
        //}

        //public override int GetHashCode()
        //{
        //    return Name.GetHashCode() ^ Price.GetHashCode();
        //}
    }
}
